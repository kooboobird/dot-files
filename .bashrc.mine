HOME_DIR=~/gitlab.com/dot-files
BASHRC_MINE=~/gitlab.com/dot-files/.bashrc.mine
VIM_CONFIG_VER=2020-11-14
TMUX_CONFIG_VER=2020-11-14

PS1='\[\033[32m\][ \u@\h \[\033[34m\]\W\[\033[32m\] ]\[\033[00m\]$ '

function setup_vim() {
    sed -n '/^### START ### .vimrc/,/^### E N D ### .vimrc/ p' ${BASHRC_MINE?} \
    | sed -e '1d; $d' -e 's/^#//' > ${HOME_DIR?}/.vimrc
}

function setup_tmux() {
    sed -n '/^### START ### .tmux.conf/,/^### E N D ### .tmux.conf/ p' ${BASHRC_MINE?} \
    | sed -e '1d; $d' -e 's/^#//' > ${HOME_DIR?}/.tmux.conf
}

### START ### .vimrc
#
#syntax on
#set encoding=utf-8
#
#set tabstop=4
#set shiftwidth=4
#set expandtab
#
#set number
#set relativenumber
#set noautoindent
#set nocindent
#set nosmartindent
#set indentexpr=
#set hlsearch
#set mouse=a
#
#"set cursorline
#"hi CursorLine term=NONE cterm=NONE
#"hi CursorLineNr ctermbg=23 ctermfg=Yellow term=NONE cterm=NONE
#
#set laststatus=2
#set statusline=\ %f%=%p%%\ %l:%c\ [%{&fileformat};%{&fileencoding?&fileencoding:&encoding}]\ \ 
#
#let g:netrw_banner=0
#let g:netrw_liststyle=3
#
#" Moving around among panes.
#nnoremap <C-J> <C-W><C-J>
#nnoremap <C-K> <C-W><C-K>
#nnoremap <C-H> <C-W><C-H>
#nnoremap <C-L> <C-W><C-L>
### E N D ### .vimrc

### START ### .tmux.conf
#
#set -g prefix C-q
#set -g mouse on
#
## Focus & Resize
#set -s escape-time 0
#bind-key -T root M-h select-pane -L
#bind-key -T root M-j select-pane -D
#bind-key -T root M-k select-pane -U
#bind-key -T root M-l select-pane -R
#
#bind-key -T root M-y resize-pane -L
#bind-key -T root M-u resize-pane -D
#bind-key -T root M-i resize-pane -U
#bind-key -T root M-o resize-pane -R
#
#bind-key -T root M-f resize-pane -Z
#
## Select Windows
#bind-key -T root M-0 select-window -t :=0
#bind-key -T root M-1 select-window -t :=1
#bind-key -T root M-2 select-window -t :=2
#bind-key -T root M-3 select-window -t :=3
#bind-key -T root M-4 select-window -t :=4
#bind-key -T root M-5 select-window -t :=5
#bind-key -T root M-6 select-window -t :=6
#bind-key -T root M-7 select-window -t :=7
#bind-key -T root M-8 select-window -t :=8
#bind-key -T root M-9 select-window -t :=9
#
## Switch Windows
#bind-key -T root M-n previous-window
#bind-key -T root M-m next-window
#
## Move Windows
#bind-key -T root M-v swap-window -t -1 \; select-window -t -1
#bind-key -T root M-b swap-window -t +1 \; select-window -t +1
#
## Layout
#bind-key -T root M-e next-layout
#bind-key -T root M-r rotate-window
#
## New Windows
#bind-key -T root M-c new-window
#bind-key -T root M-x kill-window
#bind-key -T root C-j split-window
#bind-key -T root C-l split-window -h
#
## Copy Mode
#set -g mode-keys vi
#bind-key -T root C-Space copy-mode
#bind-key -T copy-mode-vi v send-keys -X begin-selection
#bind-key -T copy-mode-vi y send-keys -X copy-pipe-and-cancel "xclip -i -f -selection primary | xclip -i -selection clipboard" # this is for X
##bind-key -T copy-mode-vi y send-keys -X copy-selection-and-cancel \; save-buffer /dev/clipboard # this is for msys2 in mintty
#bind-key -T root M-p paste-buffer
#
## Status Bar
#set-window-option -g window-status-current-style bg=yellow
#set-option -g status-left-length 40
#set-option -g status-left " #H [#S] "
#set-option -g status-right-length 30
#set-option -g status-right "#{?window_bigger,[#{window_offset_x}#,#{window_offset_y}] ,} %H:%M (%Z) %d-%b-%y "
### E N D ### .tmux.conf
